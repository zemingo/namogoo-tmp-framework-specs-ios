#
# Be sure to run `pod lib lint NamogooSDK.podspec' to ensure this is a
# valid spec before submitting.
#

Pod::Spec.new do |s|
  s.name             = 'NamogooSDK'
  s.version          = '0.0.38'
  s.summary          = 'An SDK tracker.'

  s.description      = <<-DESC
  An SDK tracker for other SDKs.
  DESC

  s.homepage         = 'https://bitbucket.org/zemingo/namogoo-poc-pod-ios.git'
  s.license          = { :type => 'Private', :file => 'LICENSE' }
  s.author           = { 'Ofir Zucker' => 'ofir@zemingo.com' }
  s.source           = { :http => 'https://bitbucket.org/zemingo/namogoo-framework-ios/downloads/NamogooSDK_0_0_38.zip' }
  s.vendored_frameworks = 'NamogooSDK.framework'
  s.ios.deployment_target = '10.0'
  s.swift_version = '4.2'
  s.requires_arc = true
  s.dependency 'Moya'

end
